import time
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

driver = webdriver.Chrome()
driver.get('https://gabrielecirulli.github.io/2048/')

html_elem = driver.find_element_by_tag_name('html')

while True:
    html_elem.send_keys(Keys.UP)
    time.sleep(0.1)
    html_elem.send_keys(Keys.RIGHT)
    time.sleep(0.1)
    html_elem.send_keys(Keys.DOWN)
    time.sleep(0.1)
    html_elem.send_keys(Keys.LEFT)
    time.sleep(0.1)
