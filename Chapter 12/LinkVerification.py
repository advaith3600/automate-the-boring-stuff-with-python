import requests
import bs4

target_url = ''
links = []
while len(links) == 0:
    try:
        target_url = input('Enter URL to verify links: ')
        request = requests.get(target_url)
        request.raise_for_status()
        links = bs4.BeautifulSoup(request.text, 'html.parser').select('a')
    except Exception as err:
        print(err)
        err = ''

validatedUrls = 0
for link in links:
    try:
        url = link['href']
        if url.startswith('http'):
            url_parsed = url
        elif url.startswith('//'):
            url_parsed = 'https:' + url
        elif url.startswith('#'):
            url_parsed = target_url + url

        print(f'Validating url {url_parsed}')
        result = requests.get(url_parsed)
        result.raise_for_status()

        if result.status_code == 404:
            print(f"Broken Link: {url_parsed}")
            continue

        validatedUrls += 1
    except Exception as err:
        print(err)
        continue

print('All the links have been checked')
print(f'Validated {validatedUrls}/{len(links)}')