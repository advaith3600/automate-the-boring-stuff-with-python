from pathlib import Path
import re

file = open(Path.cwd() / 'spam/madlibs.txt')
text = file.read()
file.close()

regex = re.compile('(ADJECTIVE)|(NOUN)|(VERB)')

while True:
    result = regex.search(text)
    try:
        print(f"Enter {result.group()} substitute: ")
    except AttributeError:
        break

    subst = input()
    text = regex.sub(subst, text, 1)

print(text)

file = open(Path.cwd() / 'spam/madlibs_ans.txt', 'w')
file.write(text)
file.close()