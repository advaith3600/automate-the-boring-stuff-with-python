from pathlib import Path
import re

try:
    reg = input("Enter regex: ")
    search_regex = re.compile(reg)

    for filename in list((Path.cwd() / 'spam').glob('*.txt')):
        print(filename, '\n', search_regex.findall(open(filename).read()))
except:
    print("Invalid Regex")
