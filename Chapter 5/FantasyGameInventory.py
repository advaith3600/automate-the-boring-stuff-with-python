from copy import copy


def displayInventory(inventory):
    print('Inventory:')
    total_item = 0

    for item, count in inventory.items():
        total_item += count
        print(f'{count} {item}')

    print("Total number of items: " + str(total_item))


def addToInventory(inventory, items):
    inv = copy(inventory)
    for item in items:
        inv.setdefault(item, 0)
        inv[item] += 1

    return inv


# copy - {'rope': 1, 'torch': 6, 'gold coin': 42, 'dagger': 1, 'arrow': 12}
inventory = input('Enter the player inventory (leave blank to fill dummy details): ')
if inventory == '':
    inventory = {'rope': 1, 'torch': 6, 'gold coin': 42, 'dagger': 1, 'arrow': 12}
else:
    inventory = eval(inventory)
displayInventory(inventory)

# copy - ['gold coin', 'dagger', 'gold coin', 'gold coin', 'ruby']
new_items = input('Enter items to be added to the inventory (leave blank to fill dummy details): ')
if new_items == '':
    new_items = ['gold coin', 'dagger', 'gold coin', 'gold coin', 'ruby']
else:
    new_items = eval(new_items)
inventory = addToInventory(inventory, new_items)
displayInventory(inventory)
