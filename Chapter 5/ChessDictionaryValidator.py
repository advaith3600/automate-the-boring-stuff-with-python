import re


def chess_board_validator(board):
    if 'bking' not in board.values() or 'wking' not in board.values():
        return False

    count = {}
    for piece in board.keys():
        move = board[piece]
        count.setdefault(move, 0)
        count[move] += 1

        # piece key should be in the range 1 to 8 and must be from a-h
        # piece must either start with w or b
        # piece must valid ('pawn', 'queen', 'king', 'knight', 'bishop', 'rook')
        # queen and king must be present only 1 time
        # knight, bishop and rook must be present only 2 times
        # pawn can only be present 8 times
        if not re.compile(r'^[1-8][a-h]$').match(piece) or \
                move[0] not in ('w', 'b') or \
                move[1:] not in ('pawn', 'queen', 'king', 'knight', 'bishop', 'rook') or \
                (move in ('queen', 'king') and count[move] > 1) or \
                (move in ('knight', 'bishop', 'rook') and count[move] > 2) or \
                (move == 'pawn' and count[move] > 8):
            return False

    return True


# copy - {'1h': 'bking', '6c': 'wqueen', '2g': 'bbishop', '5h': 'bqueen', '3e': 'wking'}
chess_board = input('Enter a chess board layout (leave blank to fill dummy details): ')
if chess_board == '':
    chess_board = {'1h': 'bking', '6c': 'wqueen', '2g': 'bbishop', '5h': 'bqueen', '3e': 'wking'}
else:
    chess_board = eval(chess_board)

if chess_board_validator(chess_board):
    print('It is a valid board')
else:
    print('Invalid board entered')
