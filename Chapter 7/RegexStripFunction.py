import re


def strip(string, replace):
    find = r'\s' if replace == '' else rf'({replace})'
    return re.compile(f'^{find}+|{find}+$').sub('', string)


string = input('Enter string to be stripped: ')
replace = input('Enter character to strip: ')
print(strip(string, replace))
