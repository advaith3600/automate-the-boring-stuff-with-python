import re


def validate(password):
    return re.compile(
        r'''^
            (?=(.*[a-z])+) # atleast 1 lowercase character
            (?=(.*[A-Z])+) # atleast 1 uppercase character
            (?=(.*[0-9])+) # atleast 1 digit
            .{8,} # atleast 8 letter word
            $''',
        re.VERBOSE).match(password)


password = input('Enter a password: ')
if validate(password):
    print('Strong password')
else:
    print('Weak password')
