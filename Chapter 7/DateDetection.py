import re, sys

date = input('Enter a date (DD/MM/YYYY): ')
reg = re.compile(r'^(\d{1,2})/(\d{1,2})/(\d{4})$').search(date)

if reg is None:
    print('Date entered in invalid format')
    sys.exit()

date, month, year = reg.groups()
date, month, year = int(date), int(month), int(year)

if (month in (4, 6, 9, 11) and date > 30) or month > 12:
    print('Invalid Date')
    sys.exit()

febDays = 28
if year % 4 == 0 and (year % 100 != 0 and year % 400 == 0):
    isLeap = 29

if month == 2 and date > febDays:
    print('Invalid Date')
    sys.exit()

print('Valid date entered')