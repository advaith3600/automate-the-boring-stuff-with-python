import random
import time


def quiz():
    a = random.randint(0, 9)
    b = random.randint(0, 9)

    print(f"{a}x{b} = ?")
    ans = a * b
    chance = 0
    start_time = time.time()
    correct = False

    while chance < 3:
        chance += 1
        a = input("Ans: ")

        if time.time() - start_time > 8:
            print('Timeout')
            break

        try:
            a = int(a)
        except ValueError:
            print("Not a number")
            continue

        if a is ans:
            print("Right Answer")
            correct = True
            break
        elif chance < 3:
            print("Try Again")
        else:
            print('Out of chances')

    return correct


correct_questions = 0
for i in range(10):
    correct_questions += quiz()
    time.sleep(1)
print(f'{correct_questions}/10 correct')
