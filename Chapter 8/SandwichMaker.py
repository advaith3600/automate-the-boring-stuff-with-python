import pyinputplus as pyip

price = 0.0
print("Bread")
bread_choice = {'wheat': 2.2, 'white': 2.4, 'sourdough': 2}
bread_type = pyip.inputMenu(list(bread_choice.keys()))
price += bread_choice[bread_type]

protein_choice = {'chicken': 2, 'turkey': 3, 'ham': 2.8, 'tofu': 3.2}
protein_type = pyip.inputMenu(list(protein_choice.keys()))
price += protein_choice[protein_type]

need_cheese = pyip.inputYesNo(prompt="Want some cheese? ")
if need_cheese == "yes":
    print("Cheese")
    cheese_choice = {'cheddar': 0.45, 'Swiss': 0.55, 'mozzarella': 0.4}
    cheese_type = pyip.inputMenu(list(cheese_choice.keys()))
    price += cheese_choice[cheese_type]

need_extras = pyip.inputYesNo(prompt="Want mayo, mustard, lettuce or tomato? ")
if need_extras == 'yes':
    extras = {'mayo': 0.2, 'mustard': 0.2, 'lettuce': 0.2, 'tomato': 0.2}
    extra_type = pyip.inputMenu(list(extras.keys()))
    price += extras[extra_type]

print(f"Your sandwich is ${price}")
sandwich_num = pyip.inputInt(prompt="How many sandwiches do you want? ", min=1)

print(f"Total amount is ${sandwich_num * price}")
