# Automate the Boring Stuff with Python

Run `python Navigate.py` file to automate the stuff of opening/running all the files or individual files

### Table of contents

Chapter Number | Problems 
:---: | :---
3 | 1. CollatzSequence 
4 | 1. Comma Code <br/> 2. Coin Flip Streaks <br/> 3. Character Picture Grid
5 | 1. Chess Dictionary Validator <br/> 2. Fantasy Game Inventory
6 | 1. Table Printer
7 | 1. Date Detection <br/> 2. Strong Password Detection <br/> 3. Regex Version of the strip() Method
8 | 1. Sandwich Maker <br/> 2. Multiplication Quiz
9 | 1. Mad Libs <br/> 2. Regex Search
10 | 1. Selective Copy <br/> 2. Deleting Unneeded Files <br/> 3. Filling in the Gaps
11 | 1. Debugging Coin Toss
12 | 1. 2048 <br/> 2. Link Verification
13 | 1. Multiplication Table Maker <br/>
14 | 1. Downloading Google Forms Data <br/> 2. Converting Spreadsheets to Other Formats <br/> 3. Finding Mistakes in a Spreadsheet
15 | 1. PDF Paranoia <br/> 2. Brute-Force PDF Password Breaker