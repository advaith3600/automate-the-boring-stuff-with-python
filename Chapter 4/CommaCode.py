def join(value):
    if len(value) == 0 or len(value) == 1:
        return ''.join(value)

    string = ', '.join(value[:-1])
    string += f' and {value[-1]}'
    return string


spam = ['apples', 'bananas', 'tofu', 'cats']
print(f'List with 4 elements: {join(spam)}')

spam = ['apples', 'bananas']
print(f'List with 2 elements: {join(spam)}')

spam = ['apples']
print(f'List with 1 element: {join(spam)}')

spam = []
print(f'List with 0 elements: {join(spam)}')