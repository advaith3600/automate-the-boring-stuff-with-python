import random

numberOfStreaks = 0

for experimentNumber in range(10000):
    streak = None
    streakCount = 0
    for flip in range(100):
        rand = random.randint(0, 1)

        if rand == streak:
            streakCount += 1
        else:
            if streakCount == 6:
                numberOfStreaks += 1

            streak = rand
            streakCount = 0

print('Chance of streak: %s%%' % (numberOfStreaks / 100))
