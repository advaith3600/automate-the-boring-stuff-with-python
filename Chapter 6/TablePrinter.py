def printTable(data):
    colWidth = [0] * len(data)
    for j in range(len(data[0])):
        for i in range(len(data)):
            colWidth[i] = max(colWidth[i], len(data[i][j]))

    for j in range(len(data[0])):
        for i in range(len(data)):
            print(f'{data[i][j].rjust(colWidth[i] + 2)}', end=' ')
        print()


tableData = input('Enter table data (leave blank to fill dummy details): ')
if tableData == '':
    tableData = [['apples', 'oranges', 'cherries', 'banana'], ['Alice', 'Bob', 'Carol', 'David'],
                 ['dogs', 'cats', 'moose', 'goose']]
else:
    tableData = eval(tableData)

printTable(tableData)
