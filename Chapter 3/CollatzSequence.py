def collatz(num):
    if num % 2 == 0:
        return num // 2
    else:
        return 3 * num + 1


while True:
    try:
        number = int(input('Enter a number: '))

        if number <= 0:
            print("Please enter a natural number")
        else:
            break
    except ValueError:
        print("Please enter a valid natural number")

while number != 1:
    number = collatz(number)
    print(f'Number is {number}')
