import os
import time
import shelve
import subprocess
import pyinputplus as pyip
from pathlib import Path


# color scheme for output
class BColors:
    HEADER = '\033[95m'
    RED = '\033[91m'
    GREEN = '\033[92m'
    END = '\033[0m'

    def __getitem__(self, item):
        return self.__getattribute__(item)


class Files:
    files_cache = {}
    visited_files = {}

    def __init__(self):
        shelfFile = shelve.open('cache')
        if shelfFile.get('visited_files') is not None:
            self.visited_files = shelfFile.get('visited_files')
        shelfFile.close()

    def store(self, folder, file):
        self.visited_files.setdefault(folder, [])
        self.visited_files[folder].append(file)
        shelfFile = shelve.open('cache')
        shelfFile['visited_files'] = self.visited_files
        shelfFile.close()

    def navigate(self):
        for current in os.listdir(Path.cwd()):
            if os.path.isdir(current) and current.startswith('Chapter'):
                self.files_cache.setdefault(current, [])
                print(f'+ {BColors.HEADER}{current}{BColors.END}')

                self.navigate_subdirectory(current)

    def navigate_subdirectory(self, directory):
        for current in os.listdir(Path.cwd() / directory):
            if current.endswith('.py'):
                if current not in self.files_cache[directory]:
                    self.files_cache[directory].append(current)
                color = 'RED'
                if self.visited_files.get(directory) and current in self.visited_files.get(directory):
                    color = 'GREEN'
                print(f'  - {BColors()[color]}{current}{BColors.END}')

    def open(self, file, folder=None):
        path = None
        if folder is None:
            for folder in self.files_cache:
                if file in self.files_cache[folder]:
                    path = [folder, file]
                    break

            if path is None:
                return print(f'File not found, {file}')
        else:
            path = [folder, file]

        self.store(path[0], path[1])
        print(f'{BColors.GREEN}opening file, {Path(path[0], path[1])}{BColors.END}')
        subprocess.run(f'python "{Path(path[0], path[1])}"', shell=True)

    def open_all(self):
        for folder in self.files_cache:
            for file in self.files_cache[folder]:
                self.open(file, folder)
                time.sleep(1)

    def open_rest(self):
        for folder in self.files_cache:
            for file in self.files_cache[folder]:
                if self.visited_files.get(folder) and file in self.visited_files.get(folder):
                    continue

                self.open(file, folder)
                time.sleep(1)


files = Files()
files.navigate()
print(f'\nMarked in {BColors.GREEN}green{BColors.END} - already visited')
print(f'Marked in {BColors.RED}red{BColors.END} - not visited\n')

# print(files.visited_files, files.files_cache)
option = pyip.inputMenu(['Navigate all', 'Go to a specific file'], numbered=True)

if option == 'Navigate all':
    second_option = pyip.inputMenu(['Start over (won\'t reset the progress)', 'Continue'], numbered=True)
    if second_option != 'Continue':
        files.open_all()
    else:
        files.open_rest()
else:
    filename = input('Enter filename (eg., CollatzSequence.py): ')
    files.open(filename)
