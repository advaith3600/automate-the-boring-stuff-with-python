import openpyxl, sys
from openpyxl.styles import Font

if len(sys.argv) > 1 and sys.argv[1].isnumeric() and int(sys.argv[1]) > 0:
    num = int(sys.argv[1])
    wb = openpyxl.Workbook()
    sheet = wb.active

    for i in range(1, num + 1):
        headers = [[i+1, 1], [1, i+1]]
        for header in headers:
            cell = sheet.cell(row=header[0], column=header[1])
            cell.font = Font(bold=True)
            cell.value = i

    for i in range(num):
        for j in range(num):
            cell = sheet.cell(row=i + 2, column=j + 2)
            cell.value = (i + 1) * (j + 1)

    wb.save(f"multiplication_table_{num}.xlsx")
    print(f'Saved as multiplication_table_{num}.xlsx')
else:
    print('Enter a valid number')
