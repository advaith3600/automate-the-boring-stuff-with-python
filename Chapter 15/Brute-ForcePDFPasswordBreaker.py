import PyPDF2
from pathlib import Path


dictFile = open(Path('Chapter 15', 'spam', 'dictionary.txt'))
wordList = dictFile.readlines()
print(wordList)

filename = input('Enter filename of the encrypted file: ')
pdfReader = PyPDF2.PdfFileReader(open(filename, 'rb'))
print(pdfReader.isEncrypted)

for word in wordList:
    word = word.strip()
    if pdfReader.decrypt(word.upper()):
        print(word.upper())
        break
    elif pdfReader.decrypt(word.lower()):
        print(word.lower())
        break
