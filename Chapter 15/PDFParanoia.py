import PyPDF2
import os
import sys
from pathlib import Path

if len(sys.argv) == 2:
    password = ' '.join(sys.argv[1:])
else:
    sys.exit()

path = Path.cwd()
for folder, _, filenames in os.walk(path):
    for file in filenames:
        if file.endswith('.pdf'):
            filepath = Path(folder, file)
            pdfFileObj = open(filepath, 'rb')
            pdfReader = PyPDF2.PdfFileReader(pdfFileObj)
            print(filepath)

            if not pdfReader.isEncrypted:
                pdfWriter = PyPDF2.PdfFileWriter()
                print("not encrypted")

                for pageNum in range(pdfReader.numPages):
                    pdfWriter.addPage(pdfReader.getPage(pageNum))

                pdfWriter.encrypt(password)
                newname = file.replace('.pdf', '_encrypted.pdf')
                new = Path(folder, newname)
                resultPdf = open(new, 'wb')
                pdfWriter.write(resultPdf)
                resultPdf.close()


for folder, _, filenames in os.walk(path):
    for file in filenames:
        if file.endswith('_encrypted.pdf'):
            print(file)
            filepath = Path(folder, file)
            print(filepath)
            pdfFileObj = open(filepath, 'rb')
            pdfReader = PyPDF2.PdfFileReader(pdfFileObj)

            if pdfReader.isEncrypted:
                success = pdfReader.decrypt(password)

                if success:
                    pdfWriter = PyPDF2.PdfFileWriter()

                    for pageNum in range(pdfReader.numPages):
                        pdfWriter.addPage(pdfReader.getPage(pageNum))

                    newname = file.replace('_encrypted.pdf', '.pdf')
                    new = Path(folder, newname)
                    resultPdf = open(new, 'wb')
                    pdfWriter.write(resultPdf)
                    resultPdf.close()
                else:
                    print('wrong password provided')
