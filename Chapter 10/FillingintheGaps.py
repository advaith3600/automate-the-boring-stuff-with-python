import os
import shutil
from pathlib import Path

i = 0
for folderName, _, filenames in os.walk(Path.cwd() / Path('spam')):
    for filename in filenames:
        if 'spam' in filename and '.txt' in filename:
            i += 1
            suffix = ("00"+str(i))[-3:]
            print(Path(folderName) / Path(filename), suffix)
            shutil.move(Path(folderName) / Path(filename), Path(folderName) / Path(f'spam{suffix}.txt'))
