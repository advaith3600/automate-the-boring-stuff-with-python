import os
import shutil
from pathlib import Path

for folderName, _, filenames in os.walk(Path.cwd()):
    for filename in filenames:
        if filename.endswith('.pdf') or filename.endswith('.jpg'):
            print(filename)
            print(folderName/Path(filename), Path('/users/advaith/spam') / Path(filename))
            shutil.copy(folderName / Path(filename), Path('/users/advaith/spam'))
