import os
from pathlib import Path

for folderName, _, filenames in os.walk(Path.cwd()):
    for filename in filenames:
        size = os.path.getsize(str(Path(folderName, filename)))
        if size > 100 * 1024 * 1024 * 8:  # 100MB
            print(Path(folderName, filename), "---", size / (1024 * 1024 * 8), "MB")

print("Done")
